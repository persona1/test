<?php
/** @noinspection PhpUnused */

namespace App\Http\Controllers;

use App\Http\Requests\EntryRequest;
use App\Http\Requests\ImportRequest;
use App\Http\Requests\PostColler;

/**
 * Class ImportController
 * @package App\Http\Controllers
 */
class ImportController extends Controller
{
	private $completed;
	private $error;

	public function __construct()
	{
		$this->completed = 0;
		$this->error = 0;
	}

	/**
	 * @param ImportRequest $request
	 * @return mixed
	 */
	public function import(ImportRequest $request)
	{
		$sxObj = simplexml_load_file($request->file('xml'));
		$arFromXml = json_decode(json_encode($sxObj), true);
		$arContent = $arFromXml[array_key_first($arFromXml)];

		if (is_array($arContent) && !empty($arContent) && !isset($arContent['@attributes']))
		{
			foreach ($arContent as $v)
			{
				$this->importHelper($v);
			}
		} elseif (is_array($arContent) && !empty($arContent) && isset($arContent['@attributes']))
		{
			$this->importHelper($arContent);
		}

		return NotifyController::notify('Импорт завершен!', "Добавлено: {$this->completed} <br> Ошибок: {$this->error}");
	}

	/**
	 * @param $element
	 * @return array
	 */
	private function importHelper($element)
	{
		$res = [
			'errors' => 0,
			'completed' => 0,
		];

		$pc = new PostColler(EntryController::class, 'add', EntryRequest::class, $element['@attributes']);

		$test = $pc->call();

		if (isset($element['element']))
		{
			$element['element']['@attributes']['entry_id'] = $test->getData()->id;
			$this->importHelper($element['element']);
		}

		if (!$test)
		{
			$this->error++;
		} else
		{
			$this->completed++;
		}

		return $res;
	}
}