<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

/**
 * Class NotifyController
 * @package App\Http\Controllers
 */
class NotifyController extends Controller
{
	/**
	 * @param string $title
	 * @param string $body
	 * @param array $custom
	 * @return JsonResponse
	 */
	public static function notify(string $title, string $body, array $custom = [])
	{
		$notifyArray = array_merge([
			'title' => $title,
			'text' => $body,
		], $custom);

		return response()->json($notifyArray);
	}
}
